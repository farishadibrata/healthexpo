# Health Expo 

This project contain an API Server and vue.js project using Uikit 3.

- API Server
  - JSON Web Token installed
  - Knex.js for query builder
  - spdy for h2 / spdy request

- Vue.js

  - Vuex used

  - Vue-router

  - Uikit 3 for CSS Framework

    

## API Routes

Current version API is 1, so every route starts with /v1/

| URI     |  Method   | Auth | Body                            | Result                          |
| :------ | :-------: | :--: | :------------------------------ | ------------------------------- |
| /       | GET, POST | Yes  | -                               | Message                         |
| /login  |    GET    |  No  | -                               | Message for user                |
| /login  |   POST    |  No  | (Username / Email) and Password | An Active JWT token for an hour |
| /prot   |    GET    | Yes  | -                               | Message for user                |
| /assets |    GET    |  No  | -                               | File in asset folder            |

## Installation & Running the server

- Install node_modules

  simply run `npm install`

- Running API server

  Go to server folder and then `node index.js`

- To serve development Vue.js

  Go to client folder and then `../nodemodules/.bin/vue-cli-service serve`

## MIT License

Copyright (c) 2019 Faris Raihan Hadibrata

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.