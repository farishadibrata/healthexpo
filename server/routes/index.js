const Router = require('express').Router();

Router.use(require('./user/login').default);
Router.use(require('./user/register'));
Router.use(require('./user/activation').default);
Router.use(require('./user/reset'));
// Guarded
Router.use(require('../guard/jwt'));
Router.use(require('./prot').default);

module.exports = Router;
