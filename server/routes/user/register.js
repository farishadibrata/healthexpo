const registerRouter = require('express').Router();
const randomstring = require('randomstring');
const bcrypt = require('bcrypt');
const db = require('../../service/db');
const httpresponses = require('../../service/httpResponse');
const mail = require('../../service/email');

registerRouter.post('/register', (req, res) => {
  // validate the input
  if (!req.body.username || !req.body.password || !req.body.email || req.body.password < 6) {
    return res.status(422).json(httpresponses('notEnoughInput'));
  }
  // define the input
  const userData = {
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 12),
    email: req.body.email,
    id: randomstring.generate(),
    token: randomstring.generate(),
  };
  // Check if username exist
  db.user.getUserByEmailorUsername(req.body.username).then((username) => {
    if (username) {
      return res.status(409).json(httpresponses('usernameTaken'));
    }
    // check if email exist
    db.user.getUserByEmailorUsername(req.body.email).then((email) => {
      if (email) {
        return res.status(409).json(httpresponses('emailTaken'));
      }
      // register user and pass the activation token to email service
      db.user.registerUserData(userData).then(() => {
        db.user.registerUserToken(userData).then(() => {
          mail.sendVerification(userData.email, userData.token).then(() => res.status(201).json(httpresponses('Success')));
        });
      });
      return true;
    });
    return true;
  });
  return true;
});
module.exports = registerRouter;
