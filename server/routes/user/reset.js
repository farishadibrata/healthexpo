const randomstring = require('randomstring');
const bcrypt = require('bcrypt');
const resetRouter = require('express').Router();
const db = require('../../service/db');
const mail = require('../../service/email');
const httpresponses = require('../../service/httpResponse');

resetRouter.post('/requestreset', (req, res) => {
  if (!req.body.email) {
    return res.status(422).json(httpresponses('notEnoughInput'));
  }
  db.user.getUserByEmailorUsername(req.body.email).then((user) => {
    if (!user) {
      return res.status(401).json(httpresponses('userNotExist'));
    }
    // you can't reset password when user is not activated
    if (user.activated !== 1) {
      return res.status(401).json(httpresponses('accountIsNotActivated'));
    }
    const userData = {
      email: req.body.email,
      token: randomstring.generate(),
    };
    // Email is registered, send the reset token to the user email
    db.user.insertResetToken(userData).then(() => {
      mail.sendReset(userData.email, userData.token).then(() => res.status(201).json(httpresponses('Success')));
    });
    return true;
  });
  return true;
});
resetRouter.post('/resetpassword', (req, res) => {
  if (!req.body.token || !req.body.password || req.body.password.length < 6) {
    return res.status(422).json(httpresponses('notEnoughInput'));
  }
  db.user.getUserDataByResetToken(req.body.token).then((user) => {
    if (!user) {
      return res.status(401).json(httpresponses('invalidToken'));
    }
    // you cant use same password same as old one
    if (bcrypt.compareSync(req.body.password, user.password)) {
      return res.status(409).json(httpresponses('passwordIsSame'));
    }
    bcrypt.hash(req.body.password, 12, (err, hash) => {
      if (err) {
        throw err;
      }
      db.user.resetPassword(user.id, hash).then(() => {
        db.user.removeResetToken(req.body.token).then(() => res.status(201).json(httpresponses('Success')));
      });
    });
    return true;
  });
  return true;
});
module.exports = resetRouter;
