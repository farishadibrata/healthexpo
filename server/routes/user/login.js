import { get } from 'config';
import { sign } from 'jsonwebtoken';
import { compare } from 'bcrypt';
import { user as _user } from '../../service/db';
import httpresponses from '../../service/httpResponse';

const loginRouter = require('express').Router();

loginRouter.get('/login', (req, res) => {
  res.send('This is login page, you dont need jwt');
});

// eslint-disable-next-line consistent-return
loginRouter.post('/login', (req, res) => {
  if (req.body.email) {
    req.body.username = req.body.email;
  }
  // verify both input
  if (!req.body.username || !req.body.password || req.body.password.length < 6) {
    return res.status(422).json(httpresponses('notEnoughInput'));
  }
  // do the query
  // eslint-disable-next-line consistent-return
  _user.getUserByEmailorUsername(req.body.username).then((user) => {
    if (!user) {
      return res.status(401).json(httpresponses('authFailed'));
    }
    if (user.activated !== 1) {
      return res.status(401).json({
        token: user.token,
        message: httpresponses('accountIsNotActivated').message,
      });
    }
    compare(req.body.password, user.password).then((match) => {
      // if you sent wrong password
      if (!match) {
        return res.status(401).json(httpresponses('authFailed'));
      }
      const token = sign(
        {
          exp: Math.floor(Date.now() / 1000) + 60 * 60,
          data: {
            username: user.username,
            level: user.level,
          },
        },
        get('secret'),
      );
      // all set, give the token to the user
      return res.json({ token });
    });
  });
});

export default loginRouter;
