import { user as _user } from '../../service/db';
import httpresponses from '../../service/httpResponse';

const activationRouter = require('express').Router();

// eslint-disable-next-line consistent-return
activationRouter.post('/activate', (req, res) => {
  if (!req.body.token) {
    return res.status(422).json(httpresponses('notEnoughInput'));
  }
  // eslint-disable-next-line consistent-return
  _user.isActivationTokenValid(req.body.token).then((user) => {
    if (!user) {
      return res.status(401).json(httpresponses('invalidToken'));
    }
    if (user.activated === 1) {
      return res.status(401).json(httpresponses('invalidToken'));
    }
    _user.activateUser(user.email[0]).then(() => _user.deleteToken(req.body.token).then(() => res.status(200).json(httpresponses('Success'))));
  });
});
export default activationRouter;
