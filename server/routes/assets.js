const express = require('express');
const assets = require('express').Router();

assets.use('/assets', express.static(`${__dirname}../assets`));
module.exports = assets;
