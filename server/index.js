/* eslint-disable no-console */
const port = require('config').get('serverPort');
const app = require('express')();
const spdy = require('spdy');
const bodyParser = require('body-parser');
const fs = require('fs');

const cert = {
  key: fs.readFileSync(`${__dirname}/cert/server.key`),
  cert: fs.readFileSync(`${__dirname}/cert/server.crt`),
};

require('./service/email');

if (process.env.NODE_ENV === 'dev') {
  app.use((req, res, next) => {
    console.log(`${req.ip} : ${req.protocol} ${req.method} ${req.path}`);
    next();
  });
}

app.use(require('cors')());

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/assets', require('express').static(`${__dirname}/assets`));

app.use('/v1', require('./routes'));

app.get('*', (res) => {
  res.status(200);
});

// eslint-disable-next-line consistent-return
spdy.createServer(cert, app).listen(port, (error) => {
  if (error) {
    console.log(error);
    return process.exit(1);
  }
  console.log(`Server listening on ${port}`);
});
