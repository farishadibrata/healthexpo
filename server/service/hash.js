const bcrypt = require('bcrypt');

const hash = {
  // eslint-disable-next-line no-shadow
  verifyPassword(password, hash) {
    return bcrypt.compare(password, hash);
  },
};

module.exports = hash;
