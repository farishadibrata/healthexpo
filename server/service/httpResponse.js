// eslint-disable-next-line consistent-return
const response = (code) => {
  switch (code) {
    case 'authFailed':
      return {
        message: 'Invalid username / password',
      };
    case 'notEnoughInput':
      return {
        message: 'Unprocessable entity',
      };
    case 'invalidToken':
      return {
        message: 'Token Invalid / broken',
      };
    case 'noToken':
      return {
        message: 'Token is not supplied',
      };
    case 'accountIsNotActivated':
      return {
        message: 'Account is not activated',
      };
    case 'userExist':
      return {
        message: 'Email / Username already taken',
      };
    case 'Success':
      return {
        message: 'Success',
      };
    case 'usernameTaken':
      return {
        message: 'Username already taken',
      };
    case 'emailTaken':
      return {
        message: 'Email already taken',
      };
    case 'userNotExist':
      return {
        message: 'User is not exist',
      };
    case 'passwordIsSame':
      return {
        message: 'New password cannot be same as old',
      };
    default:
      return {
        message: 'Undefined messages',
      };
  }
};

module.exports = response;
