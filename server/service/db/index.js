const configFile = require('config').get('db');

const config = {
  client: configFile.dbType,
  connection: {
    host: configFile.server,
    user: configFile.user,
    password: configFile.password,
    database: configFile.database,
  },
  pool: { min: 0, max: 7 },
};

const knex = require('knex')(config);

knex.schema.hasTable('users').then((exists) => {
  if (!exists) {
    return knex.schema.createTable('users', (t) => {
      t.increments('id').primary();
      t.string('username');
      t.string('email');
      t.string('password');
      t.integer('level');
      t.boolean('activated');
      t.timestamps(true, true);
    });
  }
  return true;
});

knex.schema.hasTable('inactive_users').then((exists) => {
  if (!exists) {
    return knex.schema.createTable('inactive_users', (t) => {
      t.string('id');
      t.string('email');
      t.string('token');
    });
  }
  return true;
});
knex.schema.hasTable('reset_users').then((exists) => {
  if (!exists) {
    return knex.schema.createTable('reset_users', (t) => {
      t.string('email');
      t.string('token');
    });
  }
  return true;
});

const user = {
  getUserByEmailorUsername(value) {
    return knex('users')
      .leftJoin('inactive_users', 'users.email', 'inactive_users.email')
      .select()
      .where('users.email', value)
      .orWhere('users.username', value)
      .first();
  },
  getActivationTokenByEmail(value) {
    return knex('inactive_users')
      .select()
      .where({
        email: value,
      });
  },
  isActivationTokenValid(token) {
    return knex('users')
      .leftJoin('inactive_users', 'users.email', 'inactive_users.email')
      .select()
      .where('inactive_users.token', token)
      .first();
  },
  deleteToken(token) {
    return knex('inactive_users')
      .where('token', token)
      .del();
  },
  activateUser(email) {
    return knex('users')
      .where('email', email)
      .update('activated', 1);
  },
  registerUserData(userData) {
    return knex('users').insert({
      username: userData.username,
      email: userData.email,
      password: userData.password,
      level: 1,
      activated: 0,
    });
  },
  registerUserToken(userData) {
    return knex('inactive_users').insert({
      id: userData.id,
      email: userData.email,
      token: userData.token,
    });
  },
  insertResetToken(userData) {
    return knex('reset_users').insert({
      email: userData.email,
      token: userData.token,
    });
  },
  getUserDataByResetToken(token) {
    return knex('reset_users')
      .leftJoin('users', 'reset_users.email', 'users.email')
      .where('reset_users.token', token)
      .first();
  },
  resetPassword(id, newPassword) {
    return knex('users')
      .where('id', id)
      .update('password', newPassword);
  },
  removeResetToken(token) {
    return knex('reset_users')
      .where('token', token)
      .del();
  },
};

module.exports.user = user;
module.exports.knex = knex;
