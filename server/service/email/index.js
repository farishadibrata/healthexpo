/* eslint-disable no-console */
const nodemailer = require('nodemailer');
const emailConfig = require('config').get('smtp');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: emailConfig.email,
    pass: emailConfig.password,
  },
});
const option = {
  from: 'farisraihanhadibrata@gmail.com',
};

// eslint-disable-next-line consistent-return
transporter.verify((error) => {
  if (error) {
    console.log(error);
    return process.exit(1);
  }
  console.log('SMTP Setup Successful');
});

const mail = {
  sendVerification(email, token) {
    const data = {
      from: option.from,
      to: email,
      subject: 'Email verification HealthExpo',
      text: `Activation for health expo click https://127.0.0.1:3000/activate?token=${token} or enter ${token} at https://127.0.0.1:3000/activate`,
      html: `Activation for health expo <a href="https://127.0.0.1:3000/activate?token=${token}">click here </a> or enter ${token} at <a href="https://127.0.0.1:3000/activate"> activation page </a>`,
    };
    return transporter.sendMail(data);
  },
  sendReset(email, token) {
    const data = {
      from: option.from,
      to: email,
      subject: 'Reset Password HealthExpo',
      text: `Reset password for health expo click https://127.0.0.1:3000/resetpassword?token=${token} or enter ${token} at https://127.0.0.1:3000/resetpassword`,
      html: `Reset password for health expo <a href="https://127.0.0.1:3000/resetpassword?token=${token}">click here </a> or enter ${token} at <a href="https://127.0.0.1:3000/resetpassword"> reset password page </a>`,
    };
    return transporter.sendMail(data);
  },
};
module.exports = mail;
