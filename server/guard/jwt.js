
const config = require("config"),
jwt = require("jsonwebtoken"),
httpresponses = require('../service/httpResponse');
let checkToken = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
  if (token === undefined ) {
    return res.status(401).json(httpresponses("noToken"));
  }
  if (token.startsWith("Bearer ")) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.get("secret"), (err, decoded) => {
      if (err) {
        return res.status(401).json(httpresponses("invalidToken"));
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(401).json(httpresponses("noToken"));
  }
};
module.exports = [checkToken];
