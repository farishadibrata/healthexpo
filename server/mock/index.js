/* eslint-disable no-console */
import { hashSync } from 'bcrypt';
import { insert } from '../service/db';

const mockAdmin = require('config').get('mockAdmin');
const mockUser = require('config').get('mockUser');

class User {
  constructor(username, password, email, level) {
    this.username = username;
    this.password = hashSync(password, 12);
    this.email = email;
    this.level = level;
    this.activated = 1;
  }
}
const admin = new User(mockAdmin.username, mockAdmin.password, mockAdmin.email, mockAdmin.level);
const user = new User(mockUser.username, mockUser.password, mockUser.email, mockUser.level);
insert([admin, user]).into('users').then(() => {
  console.log(`Default Administrator Account : ${mockAdmin.username}/${mockAdmin.password} (${mockAdmin.email})`);
  console.log(`Default User Account : ${mockUser.username}/${mockUser.password} (${mockUser.email})`);
}).then(() => {
  process.exit();
});
